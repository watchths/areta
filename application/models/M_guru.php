<?php
class M_guru extends CI_Model
{
	function tampil()
	{
		$guru=$this->db->get('guru');
		return $guru;
	}

	function getId($data)
	{
		$param=array('nik'=>$data);
		return $this->db->get_where('guru',$param);
	}

	function simpan($data)
	{
		$this->db->insert('guru',$data);
	}

	function update($data,$id)
	{
		$this->db->where('nik',$id);
		$this->db->update('guru',$data);
	}
}