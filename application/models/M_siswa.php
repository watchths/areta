<?php
class M_siswa extends CI_Model
{
	function tampil()
	{
		$siswa=$this->db->get('siswa');
		return $siswa;

		$siswa2=$this->db->query("select*from siswa");
	}

	function getId($data)
	{
		$param=array('nim'=>$data);
		return $this->db->get_where('siswa',$param);
	}

	function simpan($data)
	{
		$this->db->insert('siswa',$data);
	}

	function update($data,$id)
	{
		$this->db->where('nim',$id);
		$this->db->update('siswa',$data);
	}
}