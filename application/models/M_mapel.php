<?php
class M_mapel extends CI_Model
{
	function tampil()
	{
		$mapel=$this->db->get('mapel');
		return $mapel;
	}

	function getId($data)
	{
		$param=array('kd_mapel'=>$data);
		return $this->db->get_where('mapel',$param);
	}

	function simpan($data)
	{
		$this->db->insert('mapel',$data);
	}

	function update($data,$id)
	{
		$this->db->where('kd_mapel',$id);
		$this->db->update('mapel',$data);
	}
}