<?php
class M_kelas extends CI_Model
{
	function tampil()
	{
		$kelas=$this->db->get('kelas');
		return $kelas;
	}

	function getId($data)
	{
		$param=array('kd_kelas'=>$data);
		return $this->db->get_where('kelas',$param);
	}

	function simpan($data)
	{
		$this->db->insert('kelas',$data);
	}

	function update($data,$id)
	{
		$this->db->where('kd_kelas',$id);
		$this->db->update('kelas',$data);
	}
}