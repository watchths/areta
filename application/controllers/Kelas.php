<?php
class Kelas extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_kelas');
	}

	public function index()
	{
		
	}
	
	function Input()
	{
		$judul="Input Data Kelas";
		$data['judul']="$judul";
		$this->template->load('media','input_kelas',$data);
	}

	function Edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Kelas";
		$data['judul']="$judul";
		$data['edit']=$this->M_kelas->getId($id)->row_array();
		$this->template->load('media','edit_kelas',$data);
	}

	function Tampil()
	{
		$judul="Tampil Data Kelas"; //judul
		$data['judul']="$judul"; //variable judul
		$data['tampil']=$this->M_kelas->tampil()->result();
		$this->template->load('media','tampil_kelas',$data);
	}

	function Simpan()
	{
		$data=array
		(
			'kd_kelas'=>$this->input->post('kd_kelas'),
			'nama'=>$this->input->post('nama'),
			'jumlah'=>$this->input->post('jumlah')
		);
		$this->M_kelas->simpan($data);
		redirect('kelas/tampil','refresh');
	}

	function Update()
	{
		$id=$this->input->post('kd_kelas');
		$data=array
		(
			'kd_kelas'=>$this->input->post('kd_kelas'),
			'nama'=>$this->input->post('nama'),
			'jumlah'=>$this->input->post('jumlah')
		);
		$this->M_kelas->update($data,$id);
		redirect('kelas/tampil','refresh');
	}

	function Hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_kelas',$id);
		$this->db->delete('kelas');
		redirect('kelas/tampil');
	}
}