<?php
class Ruang extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_ruang');
	}

	public function index()
	{
		
	}
	
	function Input()
	{
		$judul="Input Data Ruang";
		$data['judul']="$judul";
		$this->template->load('media','input_ruang',$data);
	}

	function Edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Ruang";
		$data['judul']="$judul";
		$data['edit']=$this->M_ruang->getId($id)->row_array();
		$this->template->load('media','edit_ruang',$data);
	}

	function Tampil()
	{
		$judul="Tampil Data Ruang"; //judul
		$data['judul']="$judul"; //variable judul
		$data['tampil']=$this->M_ruang->tampil()->result();
		$this->template->load('media','tampil_ruang',$data);
	}

	function Simpan()
	{
		$data=array
		(
			'kd_ruang'=>$this->input->post('kd_ruang'),
			'nama_ruang'=>$this->input->post('nama_ruang')
		);
		$this->M_ruang->simpan($data);
		redirect('ruang/tampil','refresh');
	}

	function Update()
	{
		$id=$this->input->post('kd_ruang');
		$data=array
		(
			'kd_ruang'=>$this->input->post('kd_ruang'),
			'nama_ruang'=>$this->input->post('nama_ruang')
		);
		$this->M_ruang->update($data,$id);
		redirect('ruang/tampil','refresh');
	}

	function Hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_ruang',$id);
		$this->db->delete('ruang');
		redirect('ruang/tampil');
	}
}