<?php
class Jadwal extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_jadwal','M_guru','M_kelas','M_mapel','M_ruang'));
		// $this->load->model(array('M_guru','M_kelas','M_mapel','M_ruang'));
	}

	public function index()
	{
		$judul="Tampil Data Jadwal"; //judul
		$data['judul']="$judul"; //variable judul
		$data['tampil']=$this->M_jadwal->tampil()->result();
		$this->template->load('media','jadwal/tampil',$data);
	}

	// function Tampil()
	// {
	// 	$judul="Tampil Data Jadwal"; //judul
	// 	$data['judul']="$judul"; //variable judul
	// 	$data['tampil']=$this->M_jadwal->tampil()->result();
	// 	// $data['tampil']=$this->M_siswa->tampil()->result();
	// 	// $this->load->view('tampil_siswa',$data,FALSE); //menampilkan view
	// 	$this->template->load('media','jadwal/tampil',$data);
	// }

	function Input()
	{
		$judul="Input Data Jadwal";
		$data['judul']=$judul;
		$data['nik']=$this->M_guru->tampil()->result();
		$data['kd_kelas']=$this->M_kelas->tampil()->result();
		$data['kd_mapel']=$this->M_mapel->tampil()->result();
		$data['kd_ruang']=$this->M_ruang->tampil()->result();
		$this->template->load('media','jadwal/input',$data);
	}

	function Simpan()
	{
		$data=array
		(
			'nik'=>$this->input->post('nik'),
			'jam'=>$this->input->post('jam'),
			'hari'=>$this->input->post('hari'),
			'kd_kelas'=>$this->input->post('kd_kelas'),
			'kd_mapel'=>$this->input->post('kd_mapel'),
			'kd_ruang'=>$this->input->post('kd_ruang')
		);
		$this->M_jadwal->simpan($data);
		redirect('jadwal/input','refresh');
	}


	function Tampil()
	{
		$judul="Tampil Data Jadwal"; //judul
		$data['judul']="$judul"; //variable judul
		$data['tampil']=$this->M_jadwal->tampil()->result();
		$this->template->load('media','tampil_jadwal',$data);
	}
}