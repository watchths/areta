<?php
class Guru extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_guru');
	}

	public function index()
	{
		
	}
	
	function Input()
	{
		$judul="Input Data Guru";
		$data['judul']="$judul";
		// $this->load->view('input_guru',$data,FALSE);
		$this->template->load('media','input_guru',$data);
	}

	function Edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Guru";
		$data['judul']="$judul";
		$data['edit']=$this->M_guru->getId($id)->row_array();
		// $this->load->view('edit_guru',$data,FALSE);
		$this->template->load('media','edit_guru',$data);
	}

	function Tampil()
	{
		$judul="Tampil Data Guru";
		$data['judul']="$judul";
		$data['tampil']=$this->M_guru->tampil()->result();
		//$this->load->view('tampil_guru',$data,FALSE);
		$this->template->load('media','tampil_guru',$data);
	}

	function Simpan()
	{
		$data=array
		(
			'nik'=>$this->input->post('nik'),
			'nama'=>$this->input->post('nama'),
			'email'=>$this->input->post('email'),
			'alamat'=>$this->input->post('alamat'),
			'hp'=>$this->input->post('hp'),
			'status'=>$this->input->post('status'),
			'jk'=>$this->input->post('jk'),
			'password'=>$this->input->post('password'),
			'ttl'=>$this->input->post('ttl')
		);
		$this->M_guru->simpan($data);
		redirect('guru/tampil','refresh');
	}

	function Update()
	{
		$id=$this->input->post('nik');
		$data=array
		(
			'nik'=>$this->input->post('nik'),
			'nama'=>$this->input->post('nama'),
			'email'=>$this->input->post('email'),
			'alamat'=>$this->input->post('alamat'),
			'hp'=>$this->input->post('hp'),
			'status'=>$this->input->post('status'),
			'jk'=>$this->input->post('jk'),
			'password'=>$this->input->post('password'),
			'ttl'=>$this->input->post('ttl')
		);
		$this->M_guru->update($data,$id);
		redirect('guru/tampil','refresh');
	}

	function Hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('nik',$id);
		$this->db->delete('guru');
		redirect('guru/tampil');
	}

}