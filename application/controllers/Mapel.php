<?php
class Mapel extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_mapel');
	}

	public function index()
	{
		
	}
	
	function Input()
	{
		$judul="Input Data Mapel";
		$data['judul']="$judul";
		$this->template->load('media','input_mapel',$data);
	}

	function Edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data mapel";
		$data['judul']="$judul";
		$data['edit']=$this->M_mapel->getId($id)->row_array();
		$this->template->load('media','edit_mapel',$data);
	}

	function Tampil()
	{
		$judul="Tampil Data Mapel"; //judul
		$data['judul']="$judul"; //variable judul
		$data['tampil']=$this->M_mapel->tampil()->result();
		$this->template->load('media','tampil_mapel',$data);
	}

	function Simpan()
	{
		$data=array
		(
			'kd_mapel'=>$this->input->post('kd_mapel'),
			'nama_mapel'=>$this->input->post('nama_mapel'),
			'sks'=>$this->input->post('sks')
		);
		$this->M_mapel->simpan($data);
		redirect('mapel/tampil','refresh');
	}

	function Update()
	{
		$id=$this->input->post('kd_mapel');
		$data=array
		(
			'kd_mapel'=>$this->input->post('kd_mapel'),
			'nama_mapel'=>$this->input->post('nama_mapel'),
			'sks'=>$this->input->post('sks')
		);
		$this->M_mapel->update($data,$id);
		redirect('mapel/tampil','refresh');
	}

	function Hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_mapel',$id);
		$this->db->delete('mapel');
		redirect('mapel/tampil');
	}
}