<?php
class Siswa extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_siswa');
	}

	public function index()
	{
		
	}
	
	function Input()
	{
		$judul="Input Data Siswa";
		$data['judul']="$judul";
		// $this->load->view('input_siswa',$data,FALSE);
		$this->template->load('media','input_siswa',$data);
	}

	function Edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Siswa";
		$data['judul']="$judul";
		$data['edit']=$this->M_siswa->getId($id)->row_array();
		// $this->load->view('edit_siswa',$data,FALSE);
		$this->template->load('media','edit_siswa',$data);
	}

	function Tampil()
	{
		$judul="Tampil Data Siswa"; //judul
		$data['judul']="$judul"; //variable judul
		$data['tampil']=$this->M_siswa->tampil()->result();
		// $this->load->view('tampil_siswa',$data,FALSE); //menampilkan view
		$this->template->load('media','tampil_siswa',$data);
	}

	function Simpan()
	{
		$data=array
		(
			'nim'=>$this->input->post('nim'),
			'nama'=>$this->input->post('nama'),
			'alamat'=>$this->input->post('alamat'),
			'email'=>$this->input->post('email')
		);
		$this->M_siswa->simpan($data);
		redirect('siswa/tampil','refresh');
	}

	function Update()
	{
		$id=$this->input->post('nim');
		$data=array
		(
			'nim'=>$this->input->post('nim'),
			'nama'=>$this->input->post('nama'),
			'alamat'=>$this->input->post('alamat'),
			'email'=>$this->input->post('email')
		);
		$this->M_siswa->update($data,$id);
		redirect('siswa/tampil','refresh');
	}

	function Hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('nim',$id);
		$this->db->delete('siswa');
		redirect('siswa/tampil');
	}
}