<!DOCTYPE html>
<html lang="en">
<head>
  <title>Romi</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url()?>Assets/css/bootstrap.min.css">
  <script src="<?php echo base_url()?>Assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url()?>Assets/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h1><b>MENU ADMIN</b></h1>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="<?php echo base_url(); ?>index.php/dashboard">DASHBOARD</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/siswa/tampil">SISWA</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/guru/tampil">GURU</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/mapel/tampil">MAPEL</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/ruang/tampil">RUANG</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/kelas/tampil">KELAS</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/jadwal/tampil">JADWAL</a></li>
      </ul><br>
    </div>

    <div class="col-sm-9">      
      <h3><b><?php echo $judul ?></b></h3>
      <hr>
      <p><?php echo $contents ?></p>
    </div>
  </div>
</div>

<footer class="container-fluid">
  <p>Footer Text</p>
</footer>

</body>
</html>
