<a href="<?php echo base_url() ?>jadwal/input">Tambah Jadwal</a>
<table class="table">
	<thead>
		<tr>
			<th>NO</th>
			<th>NIK</th>
			<th>NAMA</th>
			<th>JAM</th>
			<th>HARI</th>
			<th>KELAS</th>
			<th>OPSI</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no=1;
		foreach ($tampil as $key => $value) { ?>
			<tr>
				<td><?php echo $no++ ?></td>
				<td><?php echo $value->nik ?></td>
				<td><?php echo $value->nama ?></td>
				<td><?php echo $value->jam ?></td>
				<td><?php echo $value->hari ?></td>
				<td><?php echo $value->nama_kelas ?></td>
				<td><a href="<?php echo base_url()?>jadwal/edit/<?php echo $value->nik ?>">EDIT</a><a href="<?php echo base_url()?>jadwal/hapus/<?php echo $value->nik ?>">HAPUS</a></td>
			</tr>
			<?php }?>
	</tbody>
</table>