<form method="post" action="simpan">
<table class="table">
	<tr>
		<td>NIK</td>
		<td>
			<select name="nik">
				<?php foreach ($nik as $key => $nik) { ?>
				<option value="<?php echo $nik->nik?>"> <?php echo $nik->nama ?> </option>
				<?php } ?>
			</select>
		</td>
	</tr>

	<tr>
		<td>JAM</td>
		<td><input type="text" required placeholder="masukan jam" name="jam"></td>
	</tr>

	<tr>
		<td>HARI</td>
		<td><input type="text" required placeholder="masukan hari" name="hari"></td>
	</tr>

	<tr>
		<td>KELAS</td>
		<td>
			<select name="kd_kelas">
				<?php foreach ($kd_kelas as $key => $kls) { ?>
				<option value="<?php echo $kls->kd_kelas?>"> <?php echo $kls->nama ?> </option>
				<?php } ?>
			</select>
		</td>
	</tr>

	<tr>
		<td>MAPEL</td>
		<td>
			<select name="kd_mapel">
				<?php foreach ($kd_mapel as $key => $mpl) { ?>
				<option value="<?php echo $mpl->kd_mapel?>"> <?php echo $mpl->nama_mapel ?> </option>
				<?php } ?>
			</select>
		</td>
	</tr>

	<tr>
		<td>RUANG</td>
		<td>
			<select name="kd_ruang">
				<?php foreach ($kd_ruang as $key => $rng) { ?>
				<option value="<?php echo $rng->kd_ruang?>"> <?php echo $rng->nama_ruang ?> </option>
				<?php } ?>
			</select>
		</td>
	</tr>

	<tr>
		<td colspan=2><input type="submit" value="Simpan Jadwal" name="simpan"></td>
	</tr>
</table>