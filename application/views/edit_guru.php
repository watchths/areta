<<h2><?php echo $judul ?></h2>
<?php echo form_open('guru/update'); ?>
<table class="table">
	<tr>
		<td>NIK</td>
		<td><input type="text" name="nik" value="<?php echo $edit['nik']?>"></td>
	</tr>

	<tr>
		<td>NAMA</td>
		<td><input type="text" name="nama" value="<?php echo $edit['nama']?>"></td>
	</tr>

	<tr>
		<td>EMAIL</td>
		<td><input type="text" name="email" value="<?php echo $edit['email']?>"></td>
	</tr>

	<tr>
		<td>ALAMAT</td>
		<td><input type="text" name="alamat"value="<?php echo $edit['alamat']?>"></td>
	</tr>

	<tr>
		<td>HP</td>
		<td><input type="text" required placeholder="masukan nim anda" name="hp" value="<?php echo $edit['hp']?>"></td>
	</tr>

	<tr>
		<td>STATUS</td>
		<td><select name="status" value="<?php echo $edit['status']?>">
			<option value="Menikah">Menikah</option>
			<option value="Belum Menikah">Belum Menikah</option>
		</td>
	</tr>

	<tr>
		<td>JK</td>
		<td><input type="radio" name="jk" value="<?php echo $edit['jk']?>">Laki-Laki<input type="radio" name="jk" value="<?php echo $edit['jk']?>">Perempuan</td>
	</tr>

	<tr>
		<td>PASSWORD</td>
		<td><input type="text" name="password" value="<?php echo $edit['password']?>"></td>
	</tr>

	<tr>
		<td>TTL</td>
		<td><input type="date" name="ttl" value="<?php echo $edit['ttl']?>"></td>
	</tr>

	<tr>
		<td><input class="btn btn-warning" type="submit" value="SAVE"></td>
		<td><input class="btn btn-danger" type="reset" value="CANCEL"></td>
	</tr>
</table>
<?php form_close(); ?>