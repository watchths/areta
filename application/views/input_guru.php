<h2><?php echo $judul ?></h2>
<form method="post" action="simpan">
<table class="table">
	<tr>
		<td>NIK</td>
		<td><input type="text" required placeholder="masukan nik anda" name="nik"></td>
	</tr>

	<tr>
		<td>NAMA</td>
		<td><input type="text" required placeholder="masukan nama anda" name="nama"></td>
	</tr>

	<tr>
		<td>EMAIL</td>
		<td><input type="text" required placeholder="masukan email anda" name="email"></td>
	</tr>

	<tr>
		<td>ALAMAT</td>
		<td><input type="text" required placeholder="masukan alamat anda" name="alamat"></td>
	</tr>

	<tr>
		<td>HP</td>
		<td><input type="text" required placeholder="masukan no hp anda" name="hp"></td>
	</tr>

	<tr>
		<td>STATUS</td>
		<td><select name="status">
			<option value="Menikah">Menikah</option>
			<option value="Belum Menikah">Belum Menikah</option>
		</td>
	</tr>

	<tr>
		<td>JENIS KELAMIN</td>
		<td><input type="radio" name="jenis_kelamin" value="L" />Laki-Laki <input type="radio" name="jk" value="P" />Perempuan</td>
	</tr>

	<tr>
		<td>PASSWORD</td>
		<td><input type="text" required placeholder="masukan password anda" name="password"></td>
	</tr>

	<tr>
		<td>TANGGAL LAHIR</td>
		<td><input type="date" name="tanggal_lahir"></td>
	</tr>

	<tr>
		<td><input class="btn btn-warning" type="submit" value="SAVE"></td>
		<td><input class="btn btn-danger" type="reset" value="CANCEL"></td>
	</tr>
</table>
</form>