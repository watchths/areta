<?php echo form_open('login/auth'); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url()?>Assets/css/bootstrap.min.css">
  <script src="<?php echo base_url()?>Assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url()?>Assets/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="col-lg-4"></div>
	<div class="col-lg-4">
		<table class="table" align="center">
		<tbody>
			<tr>
				<td colspan="2"><b><center>HALAMAN LOGIN</center></b></td>
			</tr>
			<tr>
				<td>USERNAME</td>
				<td><input type="text" name="username" value=""/></td>
			</tr>
			<tr>
				<td>PASSWORD</td>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td><input class="btn btn-warning" type="submit" value="LOGIN"></td>
				<td><input class="btn btn-danger" type="reset" value="CANCEL"></td>
			</tr>
		</tbody>
	</div>
	<div class="col-lg-4"></div>
</table>
</body>
</html>

<?php form_close()?>